package com.bravelime.activities.spinner.spinner.models;

public interface Entity {

    EntityType getEntityType();
}
