package com.bravelime.activities.spinner.spinner.dao;

import com.bravelime.activities.spinner.spinner.models.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryDao extends MongoRepository<Category, String> {

    Category findByName(String name);
}
