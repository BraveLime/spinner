package com.bravelime.activities.spinner.spinner.exception;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;

@Getter
public class ApplicationException extends Exception {
        private static final long serialVersionUID = 1L;

        private ErrorCode errorCode;
        private JsonNode errorDetails;

        public ApplicationException() {
            super();
        }

        public ApplicationException(ErrorCode errorCode) {
            this.errorCode = errorCode;
        }

        public ApplicationException(String message, ErrorCode errorCode) {
            super(message);
            this.errorCode = errorCode;
        }

        public ApplicationException(String message, Throwable cause, ErrorCode errorCode) {
            super(message, cause);
            this.errorCode = errorCode;
        }

        public ApplicationException(Throwable cause, ErrorCode errorCode) {
            super(cause);
            this.errorCode = errorCode;
        }

        public ApplicationException(String message, Throwable cause, ErrorCode errorCode, JsonNode errorDetails) {
            super(message, cause);
            this.errorCode = errorCode;
            this.errorDetails = errorDetails;
        }
}
