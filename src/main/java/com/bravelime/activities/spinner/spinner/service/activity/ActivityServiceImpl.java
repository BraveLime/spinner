package com.bravelime.activities.spinner.spinner.service.activity;

import com.bravelime.activities.spinner.spinner.dao.ActivityDao;
import com.bravelime.activities.spinner.spinner.dto.ActivitiesDto;
import com.bravelime.activities.spinner.spinner.dto.ActivityDto;
import com.bravelime.activities.spinner.spinner.models.Activity;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ActivityServiceImpl implements ActivityService{

    private static final int ACTIVITIES_QUERY_SIZE = 3;
    private final Random rand = new Random();

    private final ActivityDao activityDao;

    @Autowired
    public ActivityServiceImpl(ActivityDao activityDao) {
        this.activityDao = activityDao;
    }

    @Override
    public ActivitiesDto getRandomActivities(String category) {

        // TODO: make it via one query with limit 3 and category param
        List<Activity> activities = activityDao.findAll()
                .stream().filter(activity -> activity.getCategories().contains(category))
                .collect(Collectors.toList());

        if (activities.size() <= ACTIVITIES_QUERY_SIZE) {
            return new ActivitiesDto(category, activities);
        }

        List<Activity> activityList = Lists.newArrayList();

        for (int i = 0; i < ACTIVITIES_QUERY_SIZE; i++) {
            int randomIndex = rand.nextInt(activities.size());
            activityList.add(activities.get(randomIndex));
            activities.remove(randomIndex);
        }

        return new ActivitiesDto(category, activityList);
    }

    @Override
    public ActivityDto addActivity(Activity activity) {
        return activityDao.save(activity).toActivityDto();
    }
}
