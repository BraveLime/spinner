package com.bravelime.activities.spinner.spinner.models;

public enum EntityType {
    ACTIVITY,
    CATEGORY,
    NULL_ENTITY_TYPE
}
