package com.bravelime.activities.spinner.spinner.service.activity;


import com.bravelime.activities.spinner.spinner.dto.ActivitiesDto;
import com.bravelime.activities.spinner.spinner.dto.ActivityDto;
import com.bravelime.activities.spinner.spinner.exception.ApplicationException;
import com.bravelime.activities.spinner.spinner.models.Activity;
import com.bravelime.activities.spinner.spinner.models.Category;
import com.bravelime.activities.spinner.spinner.service.category.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ActivityBusinessServiceImpl implements ActivityBusinessService {

    private final ActivityService activityService;
    private final CategoryService categoryService;

    public ActivityBusinessServiceImpl(ActivityService activityService, CategoryService categoryService) {
        this.activityService = activityService;
        this.categoryService = categoryService;
    }

    @Override
    public ActivitiesDto getActivities() throws ApplicationException {
        final Category category = categoryService.getRandomCategory();

        return this.getActivities(category.getName());
    }

    @Override
    public ActivitiesDto getActivities(String category) {
        return activityService.getRandomActivities(category);
    }

    @Override
    public ActivityDto addActivity(Activity activity) {
        activity.getCategories().forEach(category -> categoryService.addCategory(new Category(category)));

        return activityService.addActivity(activity);
    }
}
