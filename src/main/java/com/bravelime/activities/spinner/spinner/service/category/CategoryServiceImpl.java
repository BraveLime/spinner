package com.bravelime.activities.spinner.spinner.service.category;

import com.bravelime.activities.spinner.spinner.dao.CategoryDao;
import com.bravelime.activities.spinner.spinner.exception.ApplicationException;
import com.bravelime.activities.spinner.spinner.exception.ErrorCode;
import com.bravelime.activities.spinner.spinner.models.Category;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryDao categoryDao;

    @Autowired
    public CategoryServiceImpl(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public Category getRandomCategory() throws ApplicationException {
        List<Category> categoryList = categoryDao.findAll();

        if(categoryList.isEmpty()) {
            throw new ApplicationException(ErrorCode.ITEM_NOT_FOUND);
        }

        return categoryList.get(new Random().nextInt(categoryList.size()));
    }

    @Override
    public Category addCategory(Category category) {
        Category validateCategory = categoryDao.findByName(category.getName());

        if(validateCategory != null) {
            return validateCategory;
        }

        return categoryDao.save(category);
    }
}
