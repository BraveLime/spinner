package com.bravelime.activities.spinner.spinner.service.category;

import com.bravelime.activities.spinner.spinner.exception.ApplicationException;
import com.bravelime.activities.spinner.spinner.models.Category;

public interface CategoryService {

    Category getRandomCategory() throws ApplicationException;

    Category addCategory(Category category);
}
