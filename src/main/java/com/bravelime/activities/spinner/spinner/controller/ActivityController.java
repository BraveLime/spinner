package com.bravelime.activities.spinner.spinner.controller;

import com.bravelime.activities.spinner.spinner.dto.ActivitiesDto;
import com.bravelime.activities.spinner.spinner.dto.ActivityDto;
import com.bravelime.activities.spinner.spinner.exception.ApplicationException;
import com.bravelime.activities.spinner.spinner.models.Activity;
import com.bravelime.activities.spinner.spinner.service.activity.ActivityBusinessService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ActivityController extends BaseController {

    private final ActivityBusinessService activityBusinessService;

    public ActivityController(ActivityBusinessService activityBusinessService) {
        this.activityBusinessService = activityBusinessService;
    }

    @GetMapping(value = "/spin")
    public ActivitiesDto getActivities() throws ApplicationException {

        return activityBusinessService.getActivities();
    }

    @GetMapping(value = "/activities/{category}")
    public ActivitiesDto getActivities(@PathVariable String category) {

        return activityBusinessService.getActivities(category);
    }

    @PostMapping(value = "/activity")
    public ActivityDto addCategory(@RequestBody Activity activity) {

        return activityBusinessService.addActivity(activity);
    }
}
