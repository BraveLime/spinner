package com.bravelime.activities.spinner.spinner.dto;

import lombok.Data;
import org.springframework.boot.context.properties.ConstructorBinding;

@Data
@ConstructorBinding
public class ActivityDto {

    String name;
    String description;
    String img;
    String url;

    public ActivityDto(String name, String description, String img) {
        this.name = name;
        this.description = description;
        this.img = img;
    }

    public ActivityDto(String name, String description, String img, String url) {
        this.name = name;
        this.description = description;
        this.img = img;
        this.url = url;
    }
}
