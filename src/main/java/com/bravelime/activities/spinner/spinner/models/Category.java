package com.bravelime.activities.spinner.spinner.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Category extends BaseEntity{

    String name;

    public Category() {}

    public Category(String name) {
        this.name = name;
    }
}
