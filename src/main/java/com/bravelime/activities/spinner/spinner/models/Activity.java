package com.bravelime.activities.spinner.spinner.models;

import com.bravelime.activities.spinner.spinner.dto.ActivityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class Activity extends BaseEntity {

    String name;
    String description;
    String img;
    String url;
    List<String> categories;

    public ActivityDto toActivityDto() {
        return new ActivityDto(name, description, img, url);
    }
}
