package com.bravelime.activities.spinner.spinner.dao;

import com.bravelime.activities.spinner.spinner.models.Activity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivityDao extends MongoRepository<Activity, String> {

    List<Activity> findAll();

}
