package com.bravelime.activities.spinner.spinner.service.activity;

import com.bravelime.activities.spinner.spinner.dto.ActivitiesDto;
import com.bravelime.activities.spinner.spinner.dto.ActivityDto;
import com.bravelime.activities.spinner.spinner.exception.ApplicationException;
import com.bravelime.activities.spinner.spinner.models.Activity;

public interface ActivityBusinessService {

    ActivitiesDto getActivities() throws ApplicationException;

    ActivitiesDto getActivities(String category);

    ActivityDto addActivity(Activity activity);
}

