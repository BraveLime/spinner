package com.bravelime.activities.spinner.spinner.models;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}