package com.bravelime.activities.spinner.spinner.service.activity;

import com.bravelime.activities.spinner.spinner.dto.ActivitiesDto;
import com.bravelime.activities.spinner.spinner.dto.ActivityDto;
import com.bravelime.activities.spinner.spinner.models.Activity;

public interface ActivityService {

    ActivitiesDto getRandomActivities(String category);

    ActivityDto addActivity(Activity activity);
}
