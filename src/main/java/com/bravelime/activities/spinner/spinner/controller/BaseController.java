package com.bravelime.activities.spinner.spinner.controller;

import com.bravelime.activities.spinner.spinner.exception.ApplicationException;
import com.bravelime.activities.spinner.spinner.exception.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

@Slf4j
public abstract class BaseController {

    protected void checkParameter(String name, String param) throws ApplicationException {
        if (StringUtils.isEmpty(param)) {
            throw new ApplicationException("Parameter '" + name + "' can't be empty!", ErrorCode.BAD_REQUEST_PARAMS);
        }
    }
}
