package com.bravelime.activities.spinner.spinner.dto;

import com.bravelime.activities.spinner.spinner.models.Activity;
import com.bravelime.activities.spinner.spinner.models.Category;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.assertj.core.util.Lists;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivitiesDto {

    private String category;
    private List<ActivityDto> activities;

    public ActivitiesDto(String category, List<Activity> activities) {
        this.category = category;
        this.activities = Lists.newArrayList();

        activities.forEach(activity -> this.activities.add(activity.toActivityDto()));
    }
}
