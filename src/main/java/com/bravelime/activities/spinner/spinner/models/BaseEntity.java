package com.bravelime.activities.spinner.spinner.models;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;

@Data
public class BaseEntity implements Entity {

    @Id
    private String id;

    @CreatedDate
    @Field(name = "created")
    private Date created;

    @LastModifiedDate
    @Field(name = "updated")
    private Date updated;

    @Enumerated(EnumType.STRING)
    @Field(name = "status")
    private Status status;

    public BaseEntity() {
        super();
    }

    public BaseEntity(String id) {
        super();
        this.id = id;
    }

    @Override
    public EntityType getEntityType() {
        return EntityType.NULL_ENTITY_TYPE;
    }
}

