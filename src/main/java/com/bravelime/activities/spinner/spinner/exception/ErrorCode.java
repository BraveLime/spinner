package com.bravelime.activities.spinner.spinner.exception;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ErrorCode {

    GENERAL(2),
    INVALID_ARGUMENTS(30),
    BAD_REQUEST_PARAMS(31),
    ITEM_NOT_FOUND(32),
    TOO_MANY_REQUESTS(33);

    private int errorCode;

    ErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @JsonValue
    public int getErrorCode() {
        return errorCode;
    }

}
